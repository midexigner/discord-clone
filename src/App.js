import React, { useEffect } from 'react';
import { useSelector,useDispatch } from 'react-redux';
import './App.css';
import Login from './components/Login/Login';
import Chat from './components/Chat/Chat';
import Sidebar from './components/sidebar/Sidebar';
import { auth } from './app/firebase';
import {login, logout, selectUser} from './features/userSlice'


function App() {
  const dispatch = useDispatch();
  const user =  useSelector(selectUser)

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        dispatch(
          login({
            uid: authUser.uid,
            photo: authUser.photoURL,
            email: authUser.email,
            displayName: authUser.displayName,
          })
        );
      } else {
        dispatch(logout());
      }
    });
  }, [dispatch]);
  
  return (
    <div className="app">
    {user ? (
       <>
       <Sidebar />
       <Chat />
       </>
    ):(<Login />)}
    </div>
  );
}

export default App;
