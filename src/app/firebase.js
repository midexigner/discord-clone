import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBzJ5QUEbfMg0BPGHifTjnS3nyABtNjgWw",
    authDomain: "discord-clone-9caba.firebaseapp.com",
    projectId: "discord-clone-9caba",
    storageBucket: "discord-clone-9caba.appspot.com",
    messagingSenderId: "1061805594628",
    appId: "1:1061805594628:web:86f36c5b5781883d76fd51"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
const auth = firebase.auth();
const db = firebaseApp.firestore();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;